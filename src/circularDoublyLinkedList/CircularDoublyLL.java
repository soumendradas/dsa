package circularDoublyLinkedList;

public class CircularDoublyLL {
	
	Node head;
	Node tail;
	int size;

	
	class Node{
		int value;
		Node next;
		Node prev;
		
		Node(int value){
			this.value = value;
		}
	}
	
	public CircularDoublyLL(int value) {
		head = new Node(value);
		tail = head;
		head.next = head;
		head.prev = head;
		
		size = 1;
	}
	
	public void insert(int value, int location) {
		if(size == 0) {
			new CircularDoublyLL(value);
			return;
		}
		
		Node newNode = new Node(value);
		if(location <= 0) {
			newNode.next = head;
			head.prev = newNode;
			head = newNode;
			head.prev = tail;
			tail.next = head;
		}else if(location >= size) {
			tail.next = newNode;
			newNode.prev = tail;
			newNode.next = head;
			head.prev = newNode;
			tail = newNode;
		}else {
			Node temp = head;
			for(int i = 0; i < location-1; i++) {
				temp = temp.next;
			}
			
			newNode.next = temp.next;
			temp.next.prev = newNode;
			newNode.prev = temp;
			temp.next = newNode;
			
		}
		
		size++;
	}

	@Override
	public String toString() {
		
		if(head == null) return null;
		StringBuilder builder = new StringBuilder();
		Node temp = head;
		for(int i = 0; i < size; i++) {
			builder.append(temp.value);
			if(i < size-1) {
				builder.append(" -> ");
				
			}
			temp = temp.next;
		}
		return builder.toString();
	}
	
	public String reverseTraverse() {
		if(head == null) return null;
		
		Node temp = tail;
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < size; i++) {
			builder.append(temp.value);
			if(i < size-1) {
				builder.append(" <- ");
				
			}
			temp = temp.prev;
		}
		return builder.toString();
	}
	
	public int search(int value) {
		if(head != null) {
			Node temp = head;
			for(int i = 0; i < size; i++) {
				if(temp.value == value) {
					return i;
				}
				temp = temp.next;
			}
		}
		
		return -1;
	}
	
	public Node delete(int location) {
		if(head == null) return null;
		
		if(location == 0) {
			Node temp = head;
			
			if(head == tail) {
				head = null;
				tail = null;
			}else {
				tail.next = head.next;
				head = head.next;
				head.prev = tail;
			}
		
			size--;
			return temp;
		}
		if(location == size-1) {
			Node temp = tail;
			tail = tail.prev;
			tail.next = head;
			head.prev = tail;
			size--;
			return temp;
		}
		
		if(location > 0 && location < size-1) {
			Node temp = head;
			for(int i = 0; i < location; i++) {
				temp = temp.next;
			}
			temp.prev.next = temp.next;
			temp.next.prev = temp.prev;
			
			size--;
			return temp;
		}
		
		return null;
	}
	
	
	
}
