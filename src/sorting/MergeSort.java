package sorting;

import java.util.Arrays;

public class MergeSort {
	
	private void merge(int[] arr, int left, int middle, int right) {
		int[] leftTempArr = new int[middle - left + 2];
		int[] rightTempArr = new int[right - middle + 1];
		
		for(int i = 0; i <= middle - left; i++) {
			leftTempArr[i] = arr[left+i];
		}
		
		for(int i = 0; i < right - middle; i++) {
			rightTempArr[i] = arr[middle+1+ i];
		}
		
		leftTempArr[middle - left + 1] = Integer.MAX_VALUE;
		rightTempArr[right - middle] = Integer.MAX_VALUE;
		
		int i = 0, j = 0;
		
		for(int k = left; k <= right; k++) {
			
			if(leftTempArr[i] < rightTempArr[j]) {
				arr[k] = leftTempArr[i];
				i++;
			}
			else {
				arr[k] = rightTempArr[j];
				j++;
			}
		}
		
		
	}
	
	public void mergeSort(int[] array, int left, int right) {
		if(right > left) {
			int m = (left+right)/2;
			mergeSort(array, left, m);
			mergeSort(array, m+1, right);
			merge(array, left, m, right);
		}
	}
	
	public static void main(String[] args) {
		int[] arr = {4, 3, 5, 2, 1, 6,10,9,8};
		MergeSort ms = new MergeSort();
		ms.mergeSort(arr, 0, arr.length - 1);
		System.out.println(Arrays.toString(arr));
	}
	
	

}
