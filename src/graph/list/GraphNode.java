package graph.list;

import java.util.ArrayList;
import java.util.List;

public class GraphNode {
	
	public String name;
	
	public int index;
	
	public boolean isVisited = false;
	
	public List<GraphNode> neighbours = new ArrayList<>();

	public GraphNode(String name, int index) {
		this.name = name;
		this.index = index;
	}
	
	

}
