package leetcode.stackOfPlates;

import java.util.EmptyStackException;

public class Stack {
	
	public Node top;
	public Node bottom;
	private int capacity;
	public int size = 0;
	
	
	
	class Node{
		int value;
		Node below;
		Node above;
		
		Node(int value){
			this.value = value;
		}
	}
	
	public Stack(int capacity) {
		this.capacity = capacity;
	}
	
	public boolean isFull() {
		return capacity == size;
	}
	
	public void join(Node above, Node below) {
		if(below!=null) below.above = above;
		if(above!= null) above.below = below;
	}
	
	public boolean push(int value) {
		if(size >= capacity) return false;
		
		size++;
		Node newNode = new Node(value);
		if(size==1) bottom = newNode;
		join(newNode, top);
		top = newNode;
		return true;
		
	}
	
	public int pop() {
		if(size == 0) throw new EmptyStackException();
		
		int result = top.value;
		top = top.below;
		size--;
		
		return result;
	}
	
	public int removeBottom() {
		int result = bottom.value;
		bottom = bottom.above;
		if(bottom!=null) bottom.below = null;
		size--;
		return result;
		
	}
	
	
}
