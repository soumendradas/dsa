package leetcode;

public class RemoveDuplicatesFromSortedArray {
	
	public static void main(String[] args) {
		
		int[] testArray = new int[10000];
		
		int index = 0;
		while(index < 5000) {
			testArray[index] = 10;
			index++;
		}
		
		while(index < 10000) {
			testArray[index] = 20;
			index++;
		}
		
		long startTime = System.nanoTime();
		//-------------------Don't call methods before this line------------------- 
		removeDuplicates(testArray);
		
		//-------------------Don't call methods after this line------------------- 
		long endTime = System.nanoTime();
		long duration = (endTime - startTime); // in nanoseconds
        
        // Optionally convert to milliseconds
        double durationInMillis = duration / 1_000_000.0;
		
		System.out.println("Time taken in ms: "+ durationInMillis);
		
		
	}
	
	
	public static int removeDuplicates(int[] nums) {
		
		if(nums.length == 0) {
			return 0;
		}
		
		int index = 0;
		
		for(int i = 0; i< nums.length; i++) {
			if(nums[i]!=nums[index]) {
				index++;
				nums[index] = nums[i];
			}
		}
		
		return index+1;
	}

}
