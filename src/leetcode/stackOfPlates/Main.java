package leetcode.stackOfPlates;

public class Main {
	
	public static void main(String[] args) {
		SetOfStacks ss = new SetOfStacks(3);
		
		ss.push(10);
		ss.push(20);
		ss.push(30);
		ss.push(40);
		ss.push(50);
		ss.push(60);
		ss.push(70);
		ss.push(80);
		
		System.out.println(ss.pop());
		System.out.println(ss.popAt(0));
	}

}
