package recursive;

public class Factorial {
	
	public static long factorial(int num) {
		if(num < 0) return -1;
		if(num == 0||num == 1) return 1;
		return num*factorial(num-1);
	}
	
	
	public static void main(String[] args) {
		
		long myFac = factorial(15);
		System.out.println(myFac);
		
	}

}
