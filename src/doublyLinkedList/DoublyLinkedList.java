package doublyLinkedList;

/**
 * This is for git testing
 * 
 */
public class DoublyLinkedList {
	
	private Node head;
	private Node tail;
	private int length;
	
	
	class Node{
		int value;
		Node next;
		Node prev;
		
		Node(int value){
			this.value = value;
		}
	}
	
	public DoublyLinkedList(int value) {
		Node newNode = new Node(value);
		head = newNode;
		tail = newNode;
		length = 1;
	}
	
	public void getHead() {
		System.out.println("Head: "+head.value);
	}
	
	public void getTail() {
		System.out.println("Tail: "+tail.value);
		
		
	}
	
	public void getLength() {
		System.out.println("Length: "+length);
	}
	
	public void printList() {
		if(head == null) {
			System.out.println("List is empty");
			return;
		}
		
		Node temp = head;
		while(temp != null) {
			System.out.println(temp.value);
			temp = temp.next;
		}
	}
	
	public void append(int value) {
		Node newNode = new Node(value);
		if(length == 0 || head== null) {
			head = newNode;
			tail = newNode;
		}else {
			tail.next = newNode;
			newNode.prev = tail;
			tail = newNode;
		}
		length++;
	}
	
	public Node removeLast() {
		if(length == 0) {
			return null;
		}
		Node temp = tail;
		if(length == 1) {
			head = null;
			temp = null;
		}else {
			tail = tail.prev;
			tail.next = null;
			temp.prev = null;
		}
		
		length--;
		return temp;
	}
	
	public void prepend(int value) {
		Node newNode = new Node(value);
		if(length == 0 || head == null) {
			head = newNode;
			tail = newNode;
		}else {
			newNode.next = head;
			head.prev = newNode;
			head = newNode;
		}
		length++;
	}
	
	public Node get(int index){
	    if(index < 0 || index >= length) return null;
	    Node temp = head;
	    if(index < length/2){
	        
	        for(int i = 0; i < index; i++){
	            temp = temp.next;
	        }
	        
	    }else{
	        temp = tail;
	        for(int i = length-1; i > index; i--) {
	        	temp = temp.prev;
	        }
	        
	    }
	    return temp;
	}
	
	public void swapFirstLast(){
	    if(length < 2) return;
	    int temp = head.value;
	    head.value = tail.value;
	    tail.value = temp;
	}
	
	public void reverse(){
	    if(length < 2) return;
	    Node currentNode = head;
	    Node tempNode = null;
	    while(currentNode != null){
	        tempNode = currentNode.prev;
	        currentNode.prev = currentNode.next;
	        currentNode.next = tempNode;
	        currentNode = currentNode.prev;
	    }
	    
	    tempNode = head;
	    head = tail;
	    tail = tempNode;
	    
	}
	
	public boolean isPalindrome(){
	    if(length < 2) return true;
	    Node headTemp = head;
	    Node tailTemp = tail;
	    for(int i = 0; i < length/2; i++){
	        if(headTemp.value != tailTemp.value) return false;
	        headTemp = headTemp.next;
	        tailTemp = tailTemp.prev;
	    }
	    return true;
	    
	}


}
