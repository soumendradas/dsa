package leetcode.animalShelter;

import java.util.LinkedList;

public class AnimalQueue {
	
	private LinkedList<Dog> dogs = new LinkedList<>();
	private LinkedList<Cat> cats = new LinkedList<>();
	
	private int order = 0;
	
	public void enqueue(Animal animal) {
		animal.setOrder(order);
		order++;
		
		if(animal instanceof Dog) {
			dogs.addLast((Dog)animal);
		}else if(animal instanceof Cat) {
			cats.addLast((Cat)animal);
		}
	}
	
	public int size() {
		return dogs.size()+cats.size();
	}
	
	public Dog dequeueDogs() {
		return dogs.poll();
	}
	
	public Dog peekDogs() {
		return dogs.peek();
	}
	
	public Cat dequeueCats() {
		return cats.poll();
	}
	
	public Cat peekCats() {
		return cats.peek();
	}
	
	public Animal dequeueAny() {
		if(dogs.size() == 0) {
			return dequeueCats();
		}else if(cats.size() == 0) {
			return dequeueDogs();
		}
		
		Dog dog = peekDogs();
		Cat cat = peekCats();
		
		if(dog.isOlderThan(cat)) {
			return dequeueDogs();
		}else {
			return dequeueCats();
		}
	}
	
	public Animal peekAny() {
		if(dogs.size() == 0) {
			return peekCats();
		}else if(cats.size() == 0) {
			return peekDogs();
		}
		
		Dog dog = peekDogs();
		Cat cat = peekCats();
		
		if(dog.isOlderThan(cat)) {
			return dog;
		}else {
			return cat;
		}
	}

}
