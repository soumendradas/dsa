package array;

import java.util.Arrays;

public class ArrayDemo {
	public static void main(String[] args) {
		
//		long startTime = System.nanoTime();
		
//		SingleDimenstionArray sd = new SingleDimenstionArray(5);
//		
//		sd.insert(2, 20);
//		sd.insert(1, 50);
//		sd.insert(2, 30);
//		sd.insert(5, 10);
//		
//		System.out.println(Arrays.toString(sd.arr));
//		
//		int[] arr = new int[3];
//		arr[0] = 10;
//		arr[1] = 20;
//		
//		System.out.println(Arrays.toString(arr));
//		arr[0] = 40;
//		System.out.println(Arrays.toString(arr));
//		
		// tab testing
		
		
		int[] testArray = new int[10000];
		
		int index = 0;
		while(index < 5000) {
			testArray[index] = 10;
			index++;
		}
		
		while(index < 10000) {
			testArray[index] = 20;
			index++;
		}
		
		
		
		long startTime = System.nanoTime();
//		ArrayUtil.middle(testArray);
		
		ArrayUtil.removeDuplicates(testArray);
		
		long endTime = System.nanoTime();
		
		long duration = (endTime - startTime); // in nanoseconds
        
        // Optionally convert to milliseconds
        double durationInMillis = duration / 1_000_000.0;
		
		System.out.println("Time taken in ms: "+ durationInMillis);
		
		
	}
	
	
}
