package stack;

public class StackMin {
	
	private Node top;
	private Node min;
	
	class Node{
		int value;
		Node next;
		
		Node(int value, Node next){
			this.value = value;
			this.next = next;
		}
	}
	
	public int min() {
		return min.value;
	}
	
	public void push(int value) {
		if(min == null) {
			min = new Node(value, min);
		}else if(min.value < value) {
			min = new Node(min.value, min);
		}else {
			min = new Node(value, min);
		}
		top = new Node(value, top);
	}
	
	public int pop() {
		if(top != null) {
			min = min.next;
			Node temp = top;
			top = top.next;
			return temp.value;
		}else {
			return -1;
		}
	}

}
