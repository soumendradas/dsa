package binarySearchTree;

import java.util.HashMap;
import java.util.Map;

public class Main {
	
	public static void main(String[] args) {
		BinarySearchTree bst = new BinarySearchTree();
		
		bst.insert(50);
		bst.insert(40);
		bst.insert(45);
		bst.insert(60);
		bst.insert(35);
		bst.insert(55);
		bst.insert(67);
		bst.rInsert(100);
		System.out.println(bst.minValue(bst.getRoot()));
		System.out.println(bst.getRoot().right.right.right.value);
		
		System.out.println(bst.contains(6));
		System.out.println(bst.rContains(50));
		System.out.println(bst.minValue(bst.getRoot().right));
		bst.deleteNode(55);
		System.out.println(bst.minValue(bst.getRoot().right));
		System.out.println(bst.minValue(bst.getRoot()));
		System.out.println(bst.BFS());
		System.out.println(bst.DFSPreOrder());
		System.out.println(bst.DFSPostOrder());
		System.out.println(bst.DFSInOrder());
		System.out.println(bst.kthSmallest(2));
		
		int index = 5;
		index = loop(index);
		System.out.println("Index: "+index);
		
	}
	
	private static int loop(int index) {
		int j = index;
		while(j < 13) {
			System.out.println(j);
			j++;
		}
		index = j;
		return index;
	}

}
