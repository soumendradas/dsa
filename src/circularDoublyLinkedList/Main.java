package circularDoublyLinkedList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		CircularDoublyLL cDLL = new CircularDoublyLL(10);
		cDLL.insert(20, 1);
		cDLL.insert(30, 2);
		cDLL.insert(40, 3);
		cDLL.insert(50, 4);
		cDLL.insert(60, 1);
		cDLL.insert(70, 6);
		cDLL.insert(5, 0);
		System.out.println(cDLL.toString());
		System.out.println(cDLL.reverseTraverse());
		System.out.println(cDLL.search(480));
		System.out.println(cDLL.delete(0).value);
		System.out.println(cDLL.toString());
		System.out.println(cDLL.reverseTraverse());
		

	}

}
