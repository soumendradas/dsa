package hashing;

import java.util.LinkedList;

public class DirectChaining {

	LinkedList<String>[] hashTable;
//	int maxChanSize = 5;
	
	public DirectChaining(int size) {
		hashTable = new LinkedList[size];
	}
	
	private int modASCIIHashFunction(String word, int M) {
		char ch[] = word.toCharArray();
		
		int i, sum;
		for(i =0, sum = 0; i < word.length(); i++) {
			sum+=ch[i];
		}
		return sum % M;
	}
	
	public void insertHashTable(String word) {
		int index = modASCIIHashFunction(word, hashTable.length);
		
		if(hashTable[index] == null) {
			hashTable[index] = new LinkedList<>();
			hashTable[index].add(word);
		}else {
			hashTable[index].add(word);
		}
	}
	
	public void displayHashTable() {
		if(hashTable == null) {
			System.out.println("HashTable does not exist");
			return;
		}
		
		for(int i = 0; i < hashTable.length; i++) {
			System.out.println("Index: "+ i + " Key: "+ hashTable[i]);
		}
	}
	
	public boolean search(String word) {
		int index = modASCIIHashFunction(word, hashTable.length);
		
		if(hashTable[index] != null && hashTable[index].contains(word)) {
			
			System.out.println("Word: "+ word+ " is found in the HashTable");
			return true;
		}else {
			System.out.println("Word: "+ word+ " is not found in the HashTable");
			return false;
		}
	}
	
	public void delete(String word) {
		int index = modASCIIHashFunction(word, hashTable.length);
		
		if(search(word)) {
			hashTable[index].remove(word);
			System.out.println("Word: "+ word+ " is deleted in the HashTable");
		}
	}
	
	
}
