package tree;

import java.util.ArrayList;
import java.util.List;

public class TreeNode {
	
	
	private String data;
	private List<TreeNode> children;
	
	public TreeNode(String data) {
		this.data = data;
		this.children = new ArrayList<>();
	}
	
	public void addChild(TreeNode child) {
		children.add(child);
		
	}
	
	public String print(int level) {
		
		String ret;
		ret = "   ".repeat(level)+data+"\n";
		for(TreeNode node : children) {
			ret+= node.print(level+1);
		}
		
		return ret;
	}

}
