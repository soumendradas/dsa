package hashTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
	
	public static List<List<String>> groupAnagrams(String[] strings){
		List<List<String>> strList = new ArrayList<>();
		HashMap<String, Integer> counts = new HashMap<>();
		for(String str : strings) {
			
		}
		
		
		return strList;
	}
	
	public static boolean itemInCommon(int[] array1, int[] array2) {
		HashMap<Integer, Boolean> myHashMap = new HashMap<>();
		
		for(int i: array1) {
			myHashMap.put(i, true);
		}
		for(int j: array2) {
			if(myHashMap.get(j)!=null) return true;
		}
		
		return false;
		
	}
	
	public static List<Integer> findDuplicates(int[] nums){
		List<Integer> duplicates = new ArrayList<>();
		Map<Integer, Integer> numCount = new HashMap<>();
		for(int num : nums) {
			numCount.put(num, numCount.getOrDefault(num, 0)+1);
		}
		
		for(Map.Entry<Integer, Integer> entry: numCount.entrySet()) {
			if(entry.getValue()>1) {
				duplicates.add(entry.getKey());
			}
			
		}
		
		return duplicates;
	}
	
	public static Character firstNonRepeatingChar(String str){
        
		Map<Character, Integer> charCount = new HashMap<>();
		for(char c: str.toCharArray()) {
			charCount.put(c, charCount.getOrDefault(c, 0)+1);
		}
		Character c = null;
		for(Map.Entry<Character, Integer> entry: charCount.entrySet()) {
			if(entry.getValue() ==1) {
				c = entry.getKey();
				return c;
			}
		}
		
		return c;
    }
	
	public static void main(String[] args) {
		HashTable ht = new HashTable();
		ht.set("nails", 140);
		ht.set("tile", 90);
		ht.set("lumber", 70);
		ht.set("bolts", 93);
		ht.set("screws", 41);
		ht.set("bolts", 100);
		
		ht.print();
		
		System.out.println(ht.get("bolts"));
		System.out.println(ht.keys());
		
		int[] arry1 = {2,4,5};
		int[] arry2 = {1,1,3};
		
		System.out.println(itemInCommon(arry1, arry2));
		
		int[] arry3 = {2,4,5,2,4,2,1,5};
		System.out.println(findDuplicates(arry3));
		
		System.out.println(firstNonRepeatingChar("lo"));
		
	}

}
