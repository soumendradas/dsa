package tree;

import java.util.LinkedList;
import java.util.Queue;

public class BinarySearchTree {
	
	Node root;
	
	
	class  Node{
		int value;
		Node left;
		Node right;
		
		Node(int value){
			this.value = value;
		}
	}
	
	private void insert(Node root, int value) {
		
		if(root == null) {
			root = new Node(value);
			if(this.root == null) this.root = root;
		}else if(value <= root.value) {
			if(root.left == null) {
				root.left = new Node(value);
			}else {
				insert(root.left, value);
			}
		}else {
			if(root.right == null) {
				root.right = new Node(value);
			}else {
				insert(root.right, value);
			}	
		}
	}
	
	public void insert(int value) {
		insert(root, value);
	}
	
	private void preOrderTraverse(Node root) {
		if(root == null) return;
		
		System.out.print(root.value + "->");
		preOrderTraverse(root.left);
		preOrderTraverse(root.right);
	}
	
	public void preOrderTraverse() {
		preOrderTraverse(root);
	}
	
	private void inOrder(Node root) {
		if(root == null) return;
		
		inOrder(root.left);
		System.out.print(root.value+"->");
		inOrder(root.right);
	}
	
	public void inOrder() {
		inOrder(root);
	}
	
	private void postOrder(Node root) {
		if(root == null) return;
		
		postOrder(root.left);
		postOrder(root.right);
		
		System.out.print(root.value+"->");
	}
	
	public void postOrder() {
		postOrder(root);
	}
	
	public void levelOrder() {
		if(root == null) return;
		
		Queue<Node> queue = new LinkedList<>();
		
		queue.add(root);
		while(!queue.isEmpty()) {
			Node presentNode = queue.remove();
			System.out.print(presentNode.value+"->");
			if(presentNode.left!=null) {
				queue.add(presentNode.left);
			}
			if(presentNode.right != null) {
				queue.add(presentNode.right);
			}
		}
	}
	
	private  Node search(Node root, int value) {
		if(root == null) {
			System.out.println("Value: "+value+" is not found in BST");
			return null;
		}
		if(root.value == value) {
			System.out.println("Value: "+value+" is found in BST");
			return root;
		}else if(value < root.value) {
			return search(root.left, value);
		}else {
			return search(root.right, value);
		}
		
		
		
		
	}
	
	public int search(int value) {
		return search(root, value).value;
	}
	
	private Node minimum(Node root) {
		if(root.left == null) return root;
		return minimum(root.left);
	}
	
	private Node deleteNode(Node root, int value) {
		if(root == null) {
			System.out.println("Value is not found in BST");
			return null;
		}
		else if(value < root.value) {
			root.left = deleteNode(root.left, value);
		}
		else if(value > root.value) {
			root.right = deleteNode(root.right, value);
		}
		else {
			if(root.left != null && root.right != null) {
				Node temp = root;
				Node minNodeForRight = minimum(temp.right);
				root.value = minNodeForRight.value;
				root.right = deleteNode(root.right, minNodeForRight.value);
				
			}else if(root.left!=null) {
				root = root.left;
			}else if(root.right != null) {
				root = root.right;
			}else {
				root = null;
			}
		}
		
		return root;
	}
	
	public int deleteNode(int value) {
		return deleteNode(root, value).value;
	}
	
	

}
