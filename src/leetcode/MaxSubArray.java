package leetcode;

import java.util.Arrays;

public class MaxSubArray {
	
	public static void main(String[] args) {
		
		int[] nums = {-4,-6,-2,3,2,-3,4,7,9};
		
		System.out.println(maxSubArray(nums));
		System.out.println(Arrays.toString(maxIntSubArray(nums)));
		
	}
	
	public static int maxSubArray(int[] nums) {
		
		int maxSum = nums[0];
		int currentSum = nums[0];
		
		for(int i = 0; i < nums.length; i++) {
//			currentSum += nums[i];
//			
//			if(currentSum > maxSum) {
//				maxSum = currentSum;
//			}
//			
//			if(currentSum < 0) {
//				currentSum = 0;
//			}
			
			currentSum = Math.max(nums[i], currentSum+nums[i]);
            maxSum = Math.max(currentSum, maxSum);
		}
		
		return maxSum;
	}
	
	public static int[] maxIntSubArray(int[] nums) {
		
		int maxSum = nums[0];
		int currentSum = nums[0];
		
		int startIndex = 0;
		int tempStartIndex = 0;
		int endIndex = 0;
		
		for(int i = 0; i < nums.length; i++) {
			if(nums[i]> currentSum + nums[i]) {
				currentSum = nums[i];
				tempStartIndex = i;
				
			}else {
				currentSum += nums[i];
				
			}
			
			if(currentSum > maxSum) {
				maxSum = currentSum;
				startIndex = tempStartIndex;
				endIndex = i;
			}
		}
		
		int[] result = new int[endIndex - startIndex+1];
		for(int i = startIndex; i <= endIndex; i++) {
			result[i-startIndex] = nums[i];
		}
		
		return result;
	}
	
	

}
