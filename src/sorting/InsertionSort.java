package sorting;

import java.util.Arrays;

public class InsertionSort {
	
	public void insertionSort(int[] arr) {
		int n = arr.length;
		
		for(int i = 1; i < n; i++) {
			int temp = arr[i], j = i;
			
			while(j > 0 && arr[j-1] > temp) {
				arr[j] = arr[j-1];
				j--;
			}
			arr[j] = temp;
		}
	}
	
	public static void main(String[] args) {
		int[] arr = {4, 3, 5, 2, 1, 2, -5};
		
		InsertionSort is = new InsertionSort();
		is.insertionSort(arr);
		System.out.println(Arrays.toString(arr));
	}

}
