package leetcode;

import java.util.Arrays;

/**
 * Important Question
 */
public class RotateImage {
	
	public static void main(String[] args) {
		RotateImage rt = new RotateImage();
		
		int[][] matrix = new int[][] {{1,2,3},{4,5,6},{7,8,9}};
		
		rt.rotate(matrix);
		System.out.println(Arrays.deepToString(matrix));
		
	}
	
	public void rotate(int[][] matrix) {
		if(matrix.length == 0 || matrix.length!= matrix[0].length) return;
		
		int n = matrix.length;
		
		for(int layer = 0; layer < n/2; layer++) {
			int first = layer;
			int last = n-1-layer;
			
			for(int i = first; i < last; i++) {
				int offset = i-first;
				
				int top = matrix[first][i];
				matrix[first][i] = matrix[last-offset][first];
				matrix[last-offset][first] = matrix[last][last-offset];
				matrix[last][last-offset] = matrix[i][last];
				matrix[i][last] = top;
			}
		}
	}

}
