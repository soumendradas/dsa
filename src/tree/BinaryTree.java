package tree;

import java.util.LinkedList;
import java.util.Queue;

public class BinaryTree {
	
	Node root;
	
	class Node{
		String value;
		Node left;
		Node right;
		
		Node(String value){
			this.value = value;
		}
	}

	public BinaryTree() {
		root = null;
	}
	
	public void insert(String value) {
		Node newNode = new Node(value);
		if(root == null) {
			root = newNode;
			System.out.println("Value is inserted in Root");
			return;
		}
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		while(!queue.isEmpty()) {
			Node presentNode = queue.remove();
			if(presentNode.left == null) {
				presentNode.left = newNode;
				System.out.println("Successfull inserted");
				break;
			}else if(presentNode.right == null) {
				presentNode.right = newNode;
				System.out.println("Successfull inserted");
				break;
			}else {
				queue.add(presentNode.left);
				queue.add(presentNode.right);
			}
		}
	}
	
	/**
	 * Level order traverse
	 */
	public void levelOrder() {
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		while(!queue.isEmpty()) {
			Node presentNode = queue.remove();
			System.out.print(presentNode.value+"->");
			
			if(presentNode.left != null) {
				queue.add(presentNode.left);
			}
			if(presentNode.right != null) {
				queue.add(presentNode.right);
			}
		}
	}
	
	
	//preOrder
	private void preOrderHelper(Node node) {
		if(node == null) return;
		System.out.print(node.value + "->");
		preOrderHelper(node.left);
		preOrderHelper(node.right);
	}
	
	public void preOrder() {
		preOrderHelper(root);
	}
	
	
	//InOrder traverse
	private void inOrderHelper(Node node) {
		if(node == null) return;
		inOrderHelper(node.left);
		System.out.print(node.value+"->");
		inOrderHelper(node.right);
	}
	
	public void inOrder() {
		inOrderHelper(root);
	}
	
	
	//Post Order traverse
	private void postOrderHelper(Node node) {
		if(node == null) return;
		postOrderHelper(node.left);
		postOrderHelper(node.right);
		System.out.print(node.value+"->");
	}
	public void postOrder() {
		postOrderHelper(root);
	}
	
	
	
	public boolean search(String value) {
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		
		while(!queue.isEmpty()) {
			Node presentNode = queue.remove();
			if(presentNode.value.equals(value)) return true;
			
			if(presentNode.left != null) {
				queue.add(presentNode.left);
			}
			
			if(presentNode.right != null) {
				queue.add(presentNode.right);
			}
		}
		
		return false;
	}
	
	public Node getDeepestNode() {
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		Node presentNode = null;
		while(!queue.isEmpty()) {
			presentNode = queue.remove();
			if(presentNode.left != null) {
				queue.add(presentNode.left);
			}
			
			if(presentNode.right != null) {
				queue.add(presentNode.right);
			}
		}
		
		return presentNode;
	}
	
	public void deleteDeepestNode() {
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		Node previousNode, presentNode = null;
		while(!queue.isEmpty()) {
			previousNode = presentNode;
			presentNode = queue.remove();
			if(presentNode.left == null) {
				previousNode.right = null;
			}else if(presentNode.right == null) {
				presentNode.left = null;
			}else {
				queue.add(presentNode.left);
				queue.add(presentNode.right);
			}
		}
	}
	
	public boolean deleteNode(String value) {
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		Node presentNode = null;
		while(!queue.isEmpty()) {
			presentNode = queue.remove();
			if(presentNode.value.equals(value)) {
				presentNode.value = getDeepestNode().value;
				deleteDeepestNode();
				return true;
			}else {
				queue.add(presentNode.left);
				queue.add(presentNode.right);
			}
		}
		return false;
	}
	
	public void deleteBT() {
		root = null;
		System.out.println("Binary tree deleted");
	}
	

}
