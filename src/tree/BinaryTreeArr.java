package tree;

public class BinaryTreeArr {
	
	private String[] arr;
	
	private int lastUsedIndex;
	
	
	public BinaryTreeArr(int size) {
		arr = new String[size+1];
		lastUsedIndex = 0;
	}
	
	public boolean isFull() {
		if(arr.length == lastUsedIndex+1) return true;
		return false;
	}
	
	public void insert(String value) {
		if(!isFull()) {
			arr[lastUsedIndex+1] = value;
			lastUsedIndex++;
		}
		
	}
	
	public void preOrder(int index) {
		if(index > lastUsedIndex) return;
		
		System.out.print(arr[index]+"->");
		preOrder(index*2);
		preOrder(index*2+1);
	}
	
	public void inOrder(int index) {
		if(index > lastUsedIndex) return;
		
		inOrder(index*2);
		System.out.print(arr[index]+"->");
		inOrder(index*2+1);
	}
	
	public void postOrder(int index) {
		if(index > lastUsedIndex) return;
		
		postOrder(index*2);
		postOrder(index*2+1);
		System.out.print(arr[index]+"->");
	}
	
	public void levelOrder() {
		for(int i =1; i <=lastUsedIndex; i++) {
			System.out.print(arr[i]+"->");
		}
	}
	
	public int search(String value) {
		for(int i =1; i <=lastUsedIndex; i++) {
			if(arr[i].equals(value)) {
				return i;
			}
		}
		
		return -1;
	}
	
	public void delete(String value) {
		int location = search(value);
		if(location == -1) return;
		arr[location] = arr[lastUsedIndex];
		arr[lastUsedIndex] = null;
		lastUsedIndex--;
		
	}

}
