package stack;

public class Main {
	
	public static void main(String[] args) {
		/*
		 * Stack st = new Stack(10); st.push(20); st.push(30); st.printList();
		 * System.out.println("POP: "+st.pop().value); st.printList();
		 */
		
		/*
		 * StackArray sta = new StackArray(4);
		 * 
		 * sta.push(1); sta.push(2); sta.push(3); sta.push(4); sta.push(5);
		 * 
		 * System.out.println(sta.pop()); System.out.println(sta.pop()); sta.delete();
		 * System.out.println(sta.pop());
		 */		
		
		/*
		 * ThreeInOne th = new ThreeInOne(3);
		 * 
		 * th.push(0, 1); th.push(0, 2); th.push(0, 3); th.push(1, 2); th.push(1, 1);
		 * th.push(2, 1); th.push(2, 20);
		 * 
		 * System.out.println(th.pop(0));
		 * 
		 * System.out.println(th.pop(1)); System.out.println(th.pop(2));
		 */
		
		StackMin sm = new StackMin();
		
		sm.push(3);
		sm.push(6);
		sm.push(2);
		
		System.out.println(sm.min());
		sm.pop();
		System.out.println(sm.min());
	}

}
