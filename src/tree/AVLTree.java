package tree;

import java.util.LinkedList;
import java.util.Queue;

public class AVLTree {
	
	private Node root;
	
	class Node{
		int value;
		int height;
		Node left;
		Node right;
		
		Node(int value){
			this.value = value;
		}
	}
	
	
	private void preOrder(Node root) {
		if(root == null) return;
		System.out.print(root.value+"->");
		preOrder(root.left);
		preOrder(root.right);
	}
	
	public void preOrder() {
		preOrder(root);
	}
	
	private void inOrder(Node root) {
		if(root == null) return;
		inOrder(root.left);
		System.out.print(root.value+"->");
		inOrder(root.right);
	}
	
	public void inOrder() {
		inOrder(root);
	}
	
	private void postOrder(Node root) {
		if(root == null) return;
		
		postOrder(root.left);
		postOrder(root.right);
		System.out.print(root.value+"->");
	}
	
	public void postOrder() {
		postOrder(root);
	}
	
	public void levelOrder() {
		if(root == null) return;
		Queue<Node> queue = new LinkedList<>();
		queue.add(root);
		while(!queue.isEmpty()) {
			Node presentNode = queue.remove();
			System.out.print(presentNode.value+"->");
			
			if(presentNode.left!=null) {
				queue.add(presentNode.left);
			}
			if(presentNode.right != null) {
				queue.add(presentNode.right);
			}
		}
	}
	
	private Node search(Node node, int value) {
		if(node == null) {
			System.out.println("Value: "+value+" is not found in this BST");
			return null;
		}else if(node.value == value) {
			System.out.println("Value: "+value+" is found in this BST");
			return node;
		}else if(value < node.value) {
			return search(node.left, value);
		}else{
			return search(node.right, value);
		}
	}
	
	public void search(int value) {
		search(root, value);
	}
	
	public int getHeight(Node node) {
		if(node == null) return 0;
		return node.height;
	}
	
	private Node rotateRight(Node disBalancedNode) {
		Node newRoot = disBalancedNode.left;
		disBalancedNode.left = disBalancedNode.left.right;
		newRoot.right = disBalancedNode;
		disBalancedNode.height = 1+Math.max(getHeight(disBalancedNode.left), getHeight(disBalancedNode.right));
		newRoot.height = 1+Math.max(getHeight(newRoot.left), getHeight(newRoot.right));
		
		return newRoot;
	}
	
	private Node rotateLeft(Node disBalancedNode) {
		Node newRoot = disBalancedNode.right;
		disBalancedNode.right = disBalancedNode.right.left;
		newRoot.left = disBalancedNode;
		disBalancedNode.height = 1+Math.max(getHeight(disBalancedNode.left), getHeight(disBalancedNode.right));
		newRoot.height = 1+Math.max(getHeight(newRoot.left), getHeight(newRoot.right));
		return newRoot;
	}
	
	private int getBalance(Node node) {
		if(node == null) return 0;
		return getHeight(node.left) - getHeight(node.right);
	}
	
	private Node insert(Node node, int value) {
		if(node == null) {
			Node newNode = new Node(value);
			newNode.height = 1;
			return newNode;
		}else if(value < node.value) {
			node.left = insert(node.left, value);
		}else {
			node.right = insert(node.right, value);
		}
		node.height = 1+Math.max(getHeight(node.left), getHeight(node.right));
		int balance = getBalance(node);
		
		if(balance > 1 && value < node.left.value) {
			return rotateRight(node);
		}
		
		if(balance > 1 && value > node.left.value) {
			node.left = rotateLeft(node);
			return rotateRight(node);
		}
		
		if(balance < -1 && value > node.right.value) {
			return rotateLeft(node);
		}
		
		if(balance < -1 && value < node.right.value) {
			node.right = rotateRight(node);
			return rotateLeft(node);
		}
		return node;
	}
	
	public void insert(int value) {
		root = insert(root, value);
	}
	
	public Node minimumNode(Node node) {
		if(node.left == null) return node;
		return minimumNode(node.left);
	}
	
	private Node deleteNode(Node node, int value) {
		if(node == null) {
			System.out.println("Node is not found in this BST");
			return node;
		}
		else if(value < node.value) {
			node.left = deleteNode(node.left, value);
		}else if(value > node.value) {
			node.right = deleteNode(node.right, value);
		}else {
			if(node.left!= null && node.right != null) {
				Node temp = node;
				Node minNodeForRight = minimumNode(temp.right);
				node.value = minNodeForRight.value;
				node.right = deleteNode(node.right,minNodeForRight.value);
			}else if(node.left!=null) {
				node = node.left;
			}else if(node.right != null) {
				node = node.right;
			}else {
				node = null;
				return node;
			}
		}
		
		
		//Rotation
		int balance = getBalance(node);
		
		if(balance > 1 && getBalance(node.left) >=0) {
			return rotateRight(node);
		}else if(balance > 1 && getBalance(node.left) < 0) {
			node.left = rotateLeft(node.left);
			return rotateRight(node);
		}else if(balance < -1 && getBalance(node.right) <= 0) {
			return rotateLeft(node);
		}
		
		else if(balance < -1 && getBalance(node.right)>0) {
			node.right = rotateRight(node.right);
			return rotateLeft(node);
		}
		
		
		return node;
	}
	
	public void deleteValue(int value) {
		deleteNode(root, value);
	}

}
