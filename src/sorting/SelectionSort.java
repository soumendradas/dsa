package sorting;

import java.util.Arrays;

public class SelectionSort {
	
	public void selectionSort(int[] arr) {
		int n = arr.length;
		
		for(int i = 0; i < n; i++) {
			int minimumLength = i;
			for(int j = i+1; j < n; j++) {
				if(arr[j] < arr[minimumLength]) {
					minimumLength = j;
				}
			}
			if(minimumLength != i) {
				int temp = arr[i];
				arr[i] = arr[minimumLength];
				arr[minimumLength] = temp;
			}
		}
	}
	
	public static void main(String[] args) {
		int[] arr = {4, 3, 5, 2, 1, 2, -5};
		
		SelectionSort ss = new SelectionSort();
		ss.selectionSort(arr);
		System.out.println(Arrays.toString(arr));
		
	}

}
