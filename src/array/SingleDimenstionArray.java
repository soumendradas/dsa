package array;

public class SingleDimenstionArray {

	int arr[] = null;

	public SingleDimenstionArray(int sizeOfArray) {
		arr = new int[sizeOfArray];
		for (int i = 0; i < sizeOfArray; i++) {		//--------------------O(n)
			arr[i] = Integer.MIN_VALUE;				//--------------------O(n)
		}
	}

	
	public void insert(int location, int value) {

		try {
			if (arr[location] == Integer.MIN_VALUE) {				//----------O(1)
				arr[location] = value;								//----------O(1)
				System.out.println("Successfully Inserted....");	//----------O(1)
			} else {
				System.out.println("Cell is already occupied");		//----------O(1)
			}
		} catch (ArrayIndexOutOfBoundsException e) {		
			// TODO: handle exception
			System.out.println("Invalid index to access array");	//----------O(1)
		}
	}
	
	/**
	 * Time complexity O(n)
	 * Space complexity O(1)
	 */
	public void traverseArray() {
		try {
			for(int i = 0; i < arr.length; i++) {			//----------------------O(n)
				System.out.print(arr[i]+", ");				//----------------------O(1)
			}
		}catch (ArrayIndexOutOfBoundsException e) {
			// TODO: handle exception		
			System.out.println("Array no longer exists");	//----------------------O(1)
		}
	}

}
