package graph.list;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class Graph {

	List<GraphNode> nodeList;

	public Graph(List<GraphNode> nodeList) {
		this.nodeList = nodeList;
	}
	
	
	public void addUndirectedEdge(int i, int j) {
		GraphNode first = nodeList.get(i);
		GraphNode second = nodeList.get(j);
		
		first.neighbours.add(second);
		second.neighbours.add(first);
	}
	
	public String toString() {
		StringBuilder s = new StringBuilder();
		
		for(int i = 0; i < nodeList.size(); i++) {
			s.append(nodeList.get(i).name).append(": ");
			for(int j = 0; j < nodeList.get(i).neighbours.size(); j++) {
				if(j == nodeList.get(i).neighbours.size() - 1) {
					s.append(nodeList.get(i).neighbours.get(j).name);
				}else {
					s.append(nodeList.get(i).neighbours.get(j).name).append(" -> ");
				}
			}
			s.append("\n");
		}
		
		
		return s.toString();
	}
	
	//internal
	private void bfs(GraphNode node) {
		LinkedList<GraphNode> queue = new LinkedList<>();
		queue.add(node);
		while(!queue.isEmpty()) {
			GraphNode currentNode = queue.removeFirst();
			currentNode.isVisited = true;
			System.out.print(currentNode.name + " ");
			for(GraphNode neighbor : currentNode.neighbours) {
				if(!neighbor.isVisited) {
					queue.add(neighbor);
					neighbor.isVisited = true;
				}
			}
		}
	}
	
	public void bfs() {
		for(GraphNode node : nodeList) {
			if(!node.isVisited) {
				bfs(node);
			}
		}
	}
	
	//internal
	private void dfs(GraphNode node) {
		Stack<GraphNode> stack = new Stack<>();
		stack.push(node);
		while(!stack.isEmpty()) {
			GraphNode currentNode = stack.pop();
			currentNode.isVisited = true;
			System.out.print(currentNode.name+" ");
			for(GraphNode neighbor: currentNode.neighbours) {
				if(!neighbor.isVisited) {
					stack.push(neighbor);
					neighbor.isVisited = true;
				}
			}
		}
	}
	
	public void dfs() {
		for(GraphNode node : nodeList) {
			if(!node.isVisited) {
				dfs(node);
			}
		}
	}
	
	//-------------------------------Topological Sort-----------------------------------------
	
	public void addDirectedEdge(int i, int j) {
		GraphNode firstNode = nodeList.get(i);
		GraphNode secondNode = nodeList.get(j);
		firstNode.neighbours.add(secondNode);
	}
	
	private void topologicalVisit(GraphNode node, Stack<GraphNode> stack) {
		for(GraphNode neighbour : node.neighbours) {
			if(!neighbour.isVisited) {
				topologicalVisit(neighbour, stack);
			}
		}
		
		node.isVisited = true;
		stack.push(node);
	}
	
	public void topologicalSort() {
		Stack<GraphNode> stack = new Stack<GraphNode>();
		for(GraphNode node : nodeList) {
			if(!node.isVisited) {
				topologicalVisit(node, stack);
			}
		}
		while(!stack.isEmpty()) {
			System.out.print(stack.pop().name+" ");
		}
	}
	
	
	
	
}
