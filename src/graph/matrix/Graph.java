package graph.matrix;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class Graph {

	List<GraphNode> nodeList = new ArrayList<>();
	int[][] adjacencyMatrix;
	
	public Graph(List<GraphNode> nodeList) {
		this.nodeList = nodeList;
		adjacencyMatrix = new int[nodeList.size()][nodeList.size()];
	}
	
	public void addUndirectedEdge(int i, int j) {
		adjacencyMatrix[i][j] = 1;
		adjacencyMatrix[j][i] = 1;
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("   ");
		for(int i = 0; i < nodeList.size(); i++) {
			s.append(nodeList.get(i).name).append(" ");
		}
		
		s.append("\n");
		
		for(int i = 0; i < nodeList.size(); i++) {
			s.append(nodeList.get(i).name+": ");
			for(int j: adjacencyMatrix[i]) {
				s.append((j)+" ");
			}
			s.append("\n");
		}
		
		
		return s.toString();
	}
	
	//get Neighbors
	private List<GraphNode> getNeighbors(GraphNode node){
		List<GraphNode> neighbors = new ArrayList<>();
		
		int nodeIndex = node.index;
		
		for(int i = 0; i < adjacencyMatrix.length; i++) {
			if(adjacencyMatrix[nodeIndex][i] == 1) {
				neighbors.add(nodeList.get(i));
			}
		}
		
		return neighbors;
	}
	
	private void bfsVisit(GraphNode node) {
		LinkedList<GraphNode> queue = new LinkedList<>();
		queue.add(node);
		while(!queue.isEmpty()) {
			GraphNode currentNode = queue.removeFirst();
			currentNode.isVisited = true;
			System.out.print(currentNode.name + " ");
			List<GraphNode> neighbors = getNeighbors(currentNode);
			
			for(GraphNode neighbor : neighbors) {
				if(!neighbor.isVisited) {
					queue.add(neighbor);
					neighbor.isVisited = true;
				}
			}
		}
	}
	
	public void bfs() {
		for(GraphNode node : nodeList) {
			if(!node.isVisited) {
				bfsVisit(node);
			}
		}
	}
	
	//internal
	private void dfs(GraphNode node) {
		Stack<GraphNode> stack = new Stack<>();
		stack.push(node);
		while(!stack.isEmpty()) {
			GraphNode currentNode = stack.pop();
			currentNode.isVisited = true;
			System.out.print(currentNode.name + " ");
			
			for(GraphNode neighbor : getNeighbors(currentNode)) {
				if(!neighbor.isVisited) {
					stack.push(neighbor);
					neighbor.isVisited = true;
				}
			}
		}
	}
	
	public void dfs() {
		for(GraphNode node : nodeList) {
			if(!node.isVisited) {
				dfs(node);
			}
		}
	}
	
	//-----------------------Topological Sort-------------------------------
	
	public void addDirectedEdge(int i, int j) {
		adjacencyMatrix[i][j] = 1;
	}
	
	private void topologicalVisit(GraphNode node, Stack<GraphNode> stack) {
		
		for(GraphNode neighbour : getNeighbors(node)) {
			if(!neighbour.isVisited) {
				topologicalVisit(neighbour, stack);
			}
		}
		node.isVisited = true;
		stack.push(node);
	}
	
	public void topologicalSort() {
		Stack<GraphNode> stack = new Stack<GraphNode>();
		for(GraphNode node : nodeList) {
			if(!node.isVisited) {
				topologicalVisit(node, stack);
			}
		}
		
		while(!stack.isEmpty()) {
			System.out.print(stack.pop().name+ " ");
		}
	}
	
	
}
