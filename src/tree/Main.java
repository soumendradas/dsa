package tree;

public class Main {

	public static void main(String[] args) {
		/*
		 * TreeNode drinks = new TreeNode("Drinks");
		 * 
		 * TreeNode hot = new TreeNode("Hot"); TreeNode cold = new TreeNode("Cold");
		 * 
		 * 
		 * TreeNode tea = new TreeNode("Tea"); TreeNode juice = new TreeNode("Juice");
		 * 
		 * drinks.addChild(hot); drinks.addChild(cold);
		 * 
		 * hot.addChild(tea); cold.addChild(juice);
		 * 
		 * System.out.println(drinks.print(0));
		 */
		
		/*
		 * BinaryTree bt = new BinaryTree(); bt.insert("N1"); bt.insert("N2");
		 * bt.insert("N3"); bt.insert("N4"); bt.insert("N5"); bt.insert("N6");
		 * bt.insert("N7");
		 * 
		 * 
		 * bt.levelOrder(); System.out.println(); bt.preOrder(); bt.deleteNode("N3");
		 * 
		 * System.out.println(); bt.levelOrder();
		 * 
		 * System.out.println(); bt.inOrder(); // bt.deleteBT(); System.out.println();
		 * bt.postOrder(); System.out.println(); System.out.println(bt.search("N"));
		 * System.out.println(bt.getDeepestNode().value);
		 */
		
		/*
		 * BinaryTreeArr bta = new BinaryTreeArr(9); bta.insert("N1"); bta.insert("N2");
		 * bta.insert("N3"); bta.insert("N4"); bta.insert("N5"); bta.insert("N6");
		 * bta.insert("N7"); bta.insert("N8"); bta.insert("N9");
		 * 
		 * bta.preOrder(1); System.out.println(); bta.inOrder(1); System.out.println();
		 * bta.postOrder(1); System.out.println(); bta.levelOrder();
		 * System.out.println(); System.out.println(bta.search("N5")); bta.delete("N3");
		 * bta.levelOrder();
		 */
		/*
		 * BinarySearchTree bst = new BinarySearchTree();
		 * 
		 * bst.insert(50); bst.insert(40); bst.insert(70); bst.insert(60);
		 * bst.insert(30); bst.insert(45); bst.insert(55); bst.insert(75);
		 * 
		 * bst.preOrderTraverse(); System.out.println(); bst.inOrder();
		 * 
		 * System.out.println(); bst.postOrder();
		 * 
		 * System.out.println(); bst.levelOrder();
		 * 
		 * System.out.println(); bst.search(30);
		 * 
		 * bst.deleteNode(30); bst.deleteNode(30); System.out.println(); bst.inOrder();
		 */
		
		/*
		 * AVLTree avl = new AVLTree(); avl.insert(10); avl.insert(5); avl.insert(20);
		 * avl.insert(30); avl.insert(40); avl.insert(50); avl.insert(60);
		 * avl.insert(70); avl.insert(80); avl.insert(90); avl.insert(100);
		 * avl.insert(4);
		 * 
		 * 
		 * 
		 * avl.levelOrder(); System.out.println(); avl.preOrder();
		 * 
		 * System.out.println(); avl.deleteValue(0);
		 * 
		 * avl.levelOrder();
		 */
		
		Trie trie = new Trie();
		
		trie.insert("API");
		trie.insert("APIS");
		trie.insert("AP");
		
		trie.search("AP");
		trie.delete("AP");
		trie.search("APIS");
		trie.delete("APIS");
		trie.search("APIS");
	}
}
