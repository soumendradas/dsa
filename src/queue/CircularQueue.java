package queue;

public class CircularQueue {
	
	private int[] arr;
	
	private int topOfQueue;
	
	private int beginingOfQueue;
	
	public CircularQueue(int size) {
		arr = new int[size];
		topOfQueue = -1;
		beginingOfQueue = -1;
		
	}
	
	public boolean isEmpty() {
		if(topOfQueue == -1) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean isFull() {
		if(beginingOfQueue == 0 && topOfQueue+1 == arr.length) {
			return true;
		}else if(topOfQueue+1 == beginingOfQueue) {
			return true;
		}else {
			return false;
		}
	}
	
	public void enQueue(int value) {
		if(isFull()) {
			System.out.println("Queue is Full...");
		}else if(isEmpty()) {
			beginingOfQueue = 0;
			topOfQueue++;
			arr[topOfQueue] = value;
			System.out.println("Successfull inserted "+ value+ " in the Queue");
		}else {
			if(topOfQueue+1 == arr.length) {
				topOfQueue = 0;
			}else {
				topOfQueue++;
			}
			arr[topOfQueue] = value;
			System.out.println("Successfull inserted "+ value+ " in the Queue");
		}
	}
	
	public int deQueue() {
		if(isEmpty()) {
			System.out.println("Queue is empty");
			return -1;
		}else {
			int result = arr[beginingOfQueue];
			arr[beginingOfQueue] = 0;
			if(beginingOfQueue == topOfQueue) {
				beginingOfQueue = topOfQueue = -1;
			}else if(beginingOfQueue+1 == arr.length) {
				beginingOfQueue = 0;
			}else {
				beginingOfQueue++;
			}
			return result;
		}
	}
	
	public int peek() {
		if(isEmpty()) {
			System.out.println("Queue is empty");
			return -1;
		}else {
			return arr[beginingOfQueue];
		}
	}
	
	public void deleteQueue() {
		arr = null;
		System.out.println("Queue is deleted");
	}

}
