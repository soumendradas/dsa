package heap;

public class Main {
	
	public static void main(String[] args) {
		Heap heap = new Heap();
		heap.insert(78);
		heap.insert(60);
		heap.insert(73);
		heap.insert(34);
		System.out.println(heap.getHeap());
		heap.insert(100);
		System.out.println(heap.getHeap());
		heap.insert(80);
		System.out.println(heap.getHeap());
		heap.insert(120);
		System.out.println(heap.getHeap());
		heap.remove();
		System.out.println(heap.getHeap());
		heap.remove();
		System.out.println(heap.getHeap());
		heap.remove();
		System.out.println(heap.getHeap());
	}

}
