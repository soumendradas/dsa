package stack;

public class StackArray {
	
	int[] arr;
	int topOfStack;
	
	/**
	 * Time complexity O(1)
	 * Space complexity O(n)
	 * @param size
	 */
	public StackArray(int size) {
		arr = new int[size];
		topOfStack = -1;
		System.out.println("Stack is created with size: "+ size);
	}
	
	/**
	 * Time complexity O(1)
	 * Space complexity O(1)
	 * @return
	 */
	public boolean isEmpty() {
		if(topOfStack == -1) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * Time complexity O(1)
	 * Space complexity O(1)
	 * @return
	 */
	public boolean isFull() {
		if(topOfStack == arr.length-1) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * Time complexity O(1)
	 * Space complexity O(1)
	 * @param value
	 */
	public void push(int value) {
		if(isFull()) {
			System.out.println("Stack is already full");
			return;
		}
		
		arr[topOfStack+1] = value;
		topOfStack++;
		System.out.println("value inserted successfully");
	}
	
	/**
	 * Time complexity O(1)
	 * Space complexity O(1)
	 * @return
	 */
	public int pop() {
		if(isEmpty()) {
			System.out.println("Stack is empty");
			return -1;
		}
		int topStack = arr[topOfStack];
		topOfStack--;
		return topStack;
	}
	
	/**
	 * Time complexity O(1)
	 * Space complexity O(1)
	 * @return
	 */
	public int peek() {
		if(isEmpty()) {
			System.out.println("Stack is empty");
			return -1;
		}
		return arr[topOfStack];
	}
	
	/**
	 * Time complexity O(1)
	 * Space complexity O(1)
	 */
	public void delete() {
		arr = null;
		topOfStack = -1;
		System.out.println("Stack deleted....");
	}

}
