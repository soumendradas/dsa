package leetcode;

import java.util.Stack;

public class QueueViaStacks {
	
	private Stack<Integer> newest;
	private Stack<Integer> oldest;
	
	public QueueViaStacks() {
		newest = new Stack<>();
		oldest = new Stack<>();
	}
	
	public int size() {
		return newest.size()+oldest.size();
	}
	
	public void enQueue(int value) {
		newest.push(value);
	}
	
	private void shift() {
		if(oldest.isEmpty()) {
			while(!newest.isEmpty()) {
				int oldValue = newest.pop();
				oldest.push(oldValue);
			}
		}
	}
	
	public int deQueue() {
		shift();
		return oldest.pop();
	}
	
	public int peek() {
		shift();
		return oldest.peek();
	}
	
	public static void main(String[] args) {
		QueueViaStacks q = new QueueViaStacks();
		q.enQueue(10);
		q.enQueue(20);
		q.enQueue(30);
		q.enQueue(40);
		
		System.out.println(q.deQueue());
		System.out.println(q.peek());
		System.out.println(q.deQueue());
		
		
		
	}

}
