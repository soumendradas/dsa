package queue;


public class Queue {
	
	private Node first;
	private Node last;
	private int length = 0; 
	
	
	class Node{
		int value;
		Node next;
		
		Node(int value){
			this.value = value;
		}
	}
	
	public boolean isEmpty() {
		if(first == null) {
			return true;
		}else {
			return false;
		}
	}
	
	public void enQueue(int value) {
		Node newNode = new Node(value);
		if(isEmpty()) {
			first = newNode;
			last = newNode;
		}else {
			last.next = newNode;
			last = newNode;
		}
		length++;
	}
	
	public int deQueue() {
		if(isEmpty()) {
			System.out.println("Queue is empty");
			return -1;
		}else {
			Node res = first;
			first = first.next;
			res.next = null;
			length--;
			return res.value;
		}
	}
	
	public int peek() {
		if(isEmpty()) {
			System.out.println("Queue is empty");
			return -1;
		}else {
			return first.value;
		}
	}
	
	public void deleteQueue() {
		first = null;
		last = null;
		System.out.println("Queue is deleted");
	}
	
	public int getSize() {
		return length;
	}
	
	public void printQueue() {
		if(first == null) {
			System.out.println("List is empty");
			return;
		}
		
		Node temp = first;
		while(temp != null) {
			System.out.println(temp.value);
			temp = temp.next;
		}
	}

}
