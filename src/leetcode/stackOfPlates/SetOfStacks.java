package leetcode.stackOfPlates;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;

public class SetOfStacks {
	
	private List<Stack> stacks = new ArrayList<>();
	private int capacity;
	
	public SetOfStacks(int capacity) {
		this.capacity = capacity;
	}
	
	private Stack getLastStack() {
		if(stacks.size() == 0) return null;
		return stacks.get(stacks.size()-1);
	}
	
	public void push(int value) {
		Stack lastStack = getLastStack();
		if(lastStack == null || lastStack.isFull()) {
			Stack stack = new Stack(capacity);
			stack.push(value);
			stacks.add(stack);
		}else {
			lastStack.push(value);
		}
	}
	
	public int pop() {
		Stack lastStack = getLastStack();
		if(lastStack == null) throw new EmptyStackException();
		int result = lastStack.pop();
		if(lastStack.size == 0) {
			stacks.remove(stacks.size()-1);
		}
		return result;
	}
	
	private int leftShift(int index, boolean removeTop) {
		Stack stack = stacks.get(index);
		int removeItem;
		if(removeTop) {
			removeItem = stack.pop();
		}else {
			removeItem = stack.removeBottom();
		}
		if(stack.size == 0) {
			stacks.remove(index);
		}else if (stacks.size() > index+1) {
			int v = leftShift(index+1, false);
			stack.push(v);
		}
		
		return removeItem;
	}
	
	public int popAt(int index) {
		return leftShift(index, true);
	}

}
