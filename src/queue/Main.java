package queue;

public class Main {
	
	public static void main(String[] args) {
		/*
		 * LinearQueue ll = new LinearQueue(3);
		 * 
		 * ll.enQueue(10); ll.enQueue(20); ll.enQueue(30); ll.enQueue(40);
		 * 
		 * System.out.println(ll.deQueue()); System.out.println(ll.deQueue());
		 * System.out.println(ll.deQueue()); System.out.println(ll.deQueue());
		 * 
		 * ll.enQueue(30); ll.enQueue(40);
		 * 
		 * System.out.println(ll.deQueue()); System.out.println(ll.deQueue());
		 */
		
		/*
		 * CircularQueue cq = new CircularQueue(3); boolean result = cq.isFull();
		 * 
		 * System.out.println(result);
		 * 
		 * cq.enQueue(10); cq.enQueue(20); cq.enQueue(30); cq.enQueue(40);
		 * 
		 * System.out.println(cq.deQueue()); System.out.println(cq.deQueue());
		 * System.out.println(cq.deQueue()); System.out.println(cq.deQueue());
		 * 
		 * cq.enQueue(30); cq.enQueue(40);
		 * 
		 * System.out.println(cq.deQueue()); System.out.println(cq.deQueue());
		 */
		
		Queue que = new Queue();
		
		que.enQueue(10);
		que.enQueue(20);
		que.enQueue(30);
		
		System.out.println("Size: "+que.getSize());
		System.out.println(que.deQueue());
		System.out.println(que.peek());
		System.out.println("Size: "+que.getSize());
		System.out.println(que.deQueue());
		System.out.println(que.deQueue());
		System.out.println(que.deQueue());
		que.deleteQueue();
		
		
	}

}
