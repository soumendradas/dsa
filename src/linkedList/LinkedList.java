package linkedList;

import java.util.HashSet;
import java.util.Set;

public class LinkedList {
	private Node head;
	private Node tail;
	private int length;
	
	class Node{
		int value;
		Node next;
		Node(int value) {
			this.value = value;
		}
	}
	
	public LinkedList(int value) {
		Node newNode = new Node(value);
		head = newNode;
		tail = newNode;
		length = 1;
	}
	
	public Node getHead() {
		if(head == null) {
			return null;
		}else {
			return this.head;
		}
	}
	
	public Node getTail() {
		if(tail == null) {
			return null;
		}else {
			return tail;
		}
	}
	
	
	public int getLength() {
		return length;
	}
	
	@Override
	public String toString() {
		Node temp = head;
		
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < length; i++) {
			builder.append(temp.value);
			if(i < length-1) {
				builder.append(" -> ");
			}
			temp = temp.next;
		}
		
		return builder.toString();
	}
	
	public void append(int value) {
		Node newNode = new Node(value);
		if(head == null) {
			head = newNode;
			tail = newNode;
		}
		else {
			tail.next = newNode;
			tail = tail.next;
		
		}
		length++;
	}
	
	public Node removeLast() {
		Node temp = head;
		Node pre = head;
		
		if(length == 0) {
			return null;
		}
		
		while(temp.next != null) {
			pre = temp;
			temp = temp.next;
		}
		
		tail = pre;
		tail.next = null;
		length--;
		
		if(length == 0) {
			head = null;
			tail = null;
		}
		
		return temp;
	}
	
	public void prepend(int value) {
		Node newNode = new Node(value);
		if(length == 0) {
			head = newNode;
			tail = newNode;
		}else {
			newNode.next = head;
			head = newNode;
		}
		length++;
	}
	
	public Node removeFirst() {
		if(length == 0) {
			return null;
		}
		Node temp = head;
		head = head.next;
		temp.next = null;
		length--;
		if(length == 0) {
			tail = null;
		}
		
		return temp;
	}
	
	public Node get(int index) {
		if(index < 0 || index >= length) {
			return null;
		}
		Node temp = head;
		
		for(int i = 0; i < index; i++) {
			temp = temp.next;
		}
		
		return temp;
	}
	
	public boolean set(int index, int value) {
		Node temp = get(index);
		if(temp != null) {
			temp.value = value;
			return true;
		}
		return false;
	}
	
	public boolean insert(int index, int value) {
		if(index < 0 || index > length) {
			return false;
		}
		
		if(index == 0) {
			prepend(value);
			return true;
		}
		
		if(index == length) {
			append(value);
			return true;
		}
		
		Node newNode = new Node(value);
		Node temp = get(index - 1);
		newNode.next = temp.next;
		temp.next = newNode;
		length++;
		return true;
	}
	
	public Node remove(int index) {
		if(index < 0 || index >= length) return null;
		
		if(index == 0) removeFirst();
		if(index == length - 1) removeLast();
		
		Node prev = get(index-1);
		Node temp = prev.next;
		prev.next = temp.next;
		temp.next = null;
		length--;
		
		return temp;
	}
	
	public void reverse() {
		Node temp = head;
		head = tail;
		tail = temp;
		Node after = temp.next;
		Node before = null;
		
		for(int i = 0; i < length ; i++) {
			after = temp.next;
			temp.next = before;
			before = temp;
			temp = after;
		}
	}
	/**
	 * Not using length variable
	 * @return
	 */
	public Node findMiddleNode() {
		
		Node fast = head;
		Node slow = head;
		while(fast != null && fast.next != null) {
			slow = slow.next;
			fast = fast.next.next;
		}
		
		return slow;
	}
	
	public boolean hasLoop() {
		Node first = head;
		Node slow = head;
		
		while(first != null && first.next != null) {
			slow = slow.next;
			first = first.next.next;
			if(slow == first) return true;
		}
		
		return false;
	}
	
	public Node findKthFromEnd(int k){
	    Node fast = head;
	    Node slow = head;
	    
	    for(int i = 0; i < k; i++){
	        if(fast == null){
	            return null;
	        }
	        fast = fast.next;
	    }
	    
	    while(fast != null){
	        slow = slow.next;
	        fast = fast.next;
	    }
	    
	    return slow;
	}
	
	/**
	 * tail pointer not used
	 * 
	 * @param x
	 */
	public void partitionList(int x) {
		if(head == null) return;
		
		Node dummy1 = new Node(0);
		Node dummy2 = new Node(0);
		
		Node prev1 = dummy1;
		Node prev2 = dummy2;
		
		Node current = head;
		while(current != null) {
			if(current.value < x) {
				prev1.next = current;
				prev1 = current;
			}else {
				prev2.next = current;
				prev2 = current;
			}
			current = current.next;
		}
		prev2.next = null;
		prev1.next = dummy2.next;
		head = dummy1.next;
	}
	
	public void removeDuplicates(){
		if(head == null) return;
		
        Set<Integer> set = new HashSet<>();
        
        
        while(head != null) {
        	set.add(head.value);
        	head = head.next;
        }
        set.forEach(x->append(x));
        
        
    }
	
	public int binaryToDecimal(){
        
        if(head == null) return 0;
        
        Node temp = head;
        int num = 0;
        
        for(int i = length-1; i < 0; i--){
            num = num+((int)Math.pow(2, i)*temp.value);
            
        }
        
        return num;
    }
	
	public void bubbleSort() {
		if(length < 2) return;
		
		Node sortedUntil = null;
		
		while(sortedUntil != this.head.next) {
			Node current = this.head;
			
			while(current.next != sortedUntil) {
				Node nextNode = current.next;
				if(current.value > nextNode.value) {
					int temp = current.value;
					current.value = nextNode.value;
					nextNode.value = temp;
				}
				current = current.next;
			}
			sortedUntil = current;
		}
	}
	
	public void selectionSort() {
		if(length < 2) return;
		
		Node current = head;
		
		while(current.next != null) {
			Node smallest = current;
			Node innerNode = current.next;
			while(innerNode!= null) {
				if(innerNode.value < smallest.value) {
					smallest = innerNode;
				}
				innerNode = innerNode.next;
			}
			
			if(current.value != smallest.value) {
				int temp = current.value;
				current.value = smallest.value;
				smallest.value = temp;
				
			}
			current = current.next;
		}
		tail = current;
	}
	
	public void insertionSort() {
		if(length < 2) return;
		
		Node sortedListHead = this.head;
		Node unSortedListHead = this.head.next;
		
		sortedListHead.next = null;
		
		while(unSortedListHead != null) {
			Node current = unSortedListHead;
			unSortedListHead = unSortedListHead.next;
			
			if(current.value < sortedListHead.value) {
				current.next = sortedListHead;
				sortedListHead = current;
			}else {
				Node searchPointer = sortedListHead;
				while(searchPointer.next != null && current.value > searchPointer.next.value){
                    searchPointer = searchPointer.next;
                    
                }
                current.next = searchPointer.next;
                    searchPointer.next = current;
                
            }
           
        }
         head = sortedListHead;
            
            Node temp = head;
            while(temp.next != null){
                temp = temp.next;
            }
            tail = temp;
        
    }
	
	/*
	 * Merge method
	 */
	public void merge(LinkedList otherList){
        Node otherHead = otherList.getHead();
        
        Node dummy = new Node(0);
        
        Node current = dummy;
        
        while(head != null && otherHead != null){
            if(head.value < otherHead.value){
                current.next = head;
                head = head.next;
            }else{
                current.next = otherHead;
                otherHead = otherHead.next;
            }
            current = current.next;
        }
        
        if(head != null){
            current.next = head;
            
            
        }else{
            current.next = otherHead;
            tail = otherList.getTail();
        }
        
        head = dummy.next;
        length += otherList.getLength();
        
    }
	
	public boolean searchNode(int value) {
		
		if(head == null) return false;
		
		Node temp = head;
		
		for(int i = 0; i < length; i++) {
			if(temp.value == value) {
				System.out.println("Node found in index of "+ i);
				return true;
			}
			temp = temp.next;
		}
		
		System.out.println("Node not found in this list");
		
		
		return false;
	}
	
	public String rotate(int number) {
		int index = number;
		
		if(index < 0) {
			index = index+length;
		}
		
		if(index == 0) return "No Rotation";
		
		if(index < 0 || index >= length) return null;
		
		
		for(int i = 0; i < index; i++) {
			Node temp = head;
			head = head.next;
			tail.next = temp;
			tail = temp;
			temp.next = null;
		}
		
		return "Success";
	}
	
	public void deleteDups(){
        if(head == null) return;
        Node current = head;
        Node prev = null;
        Set<Integer> set = new HashSet<>();
//        set.add(head.value);
        while(current != null) {
        	if(!set.add(current.value)) {
        		prev.next = current.next;
        		length--;
        	}else {
        		prev = current;
        	}
        	current = current.next;
        }
        
    }
	
	public LinkedList partition(LinkedList ll, int x) {
		Node current = ll.head;
		ll.tail = ll.head;
		while(current!=null) {
			Node next = current.next;
			
			if(current.value < x) {
				current.next = ll.head;
				ll.head = current;
			}else {
				ll.tail.next = current;
				ll.tail = current;
			}
			current = next;
		}
		ll.tail.next = null;
		return ll;
	}
    
}



