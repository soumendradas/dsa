package circularLinkedList;

public class MainMethod {

	
	public static void main(String[] args) {
		CircularLinkedList cll = new CircularLinkedList(10);
		cll.insert(6, 0);
		cll.insert(8, 1);
		cll.insert(7, 0);
		cll.insert(9, 0);
		cll.insert(5, 2);
		
//		System.out.println(cll.head.value);
//		System.out.println(cll.head.next.value);
		cll.traverse();
		System.out.println();
		cll.search(10);
		cll.deleteNode(8);
		cll.traverse();
		cll.deleteCLL();
		cll.traverse();
	}
}
