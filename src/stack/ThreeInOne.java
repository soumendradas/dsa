package stack;

public class ThreeInOne {
	private final int numberOfStacks = 3;
	private int stackCapacity;
	private int[] values;
	private int[] sizes;
	
	public ThreeInOne(int stackSize) {
		stackCapacity = stackSize;
		values = new int[stackCapacity*numberOfStacks];
		sizes = new int[numberOfStacks];
	}
	
	public boolean isFull(int stackNum) {
		if(sizes[stackNum] == stackCapacity) return true;
		return false;
	}
	
	public boolean isEmpty(int stackNum) {
		if(sizes[stackNum] == 0) return true;
		return false;
	}
	
	private int indexOfTop(int stackNum) {
		int offset = stackCapacity * stackNum;
		int size = sizes[stackNum];
		
		return offset + size - 1;
	}
	
	public void push(int stackNum, int value) {
		if(isFull(stackNum)) {
			System.out.println("Stack is full");
			
		}else {
			int topIndex = indexOfTop(stackNum);
			values[topIndex+1] = value;
			sizes[stackNum]++;
			System.out.println(value+ " is inserted");
		}
	}
	
	public int pop(int stackNum) {
		if(isEmpty(stackNum)) {
			System.out.println("Stock is empty");
			return -1;
		}else {
			int topIndex = indexOfTop(stackNum);
			int temp = values[topIndex];
			values[topIndex] = 0;
			sizes[stackNum]--;
			return temp;
		}
	}
	
	public int peek(int stackNum) {
		if(isEmpty(stackNum)) {
			System.out.println("Stock is empty");
			return -1;
		}else {
			int topIndex = indexOfTop(stackNum);
			int temp = values[topIndex];
			return temp;
		}
	}
}
