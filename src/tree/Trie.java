package tree;

import java.util.HashMap;
import java.util.Map;

public class Trie {
	private Node root;
	
	class Node{
		Map<Character, Node> children;
		boolean endOfString;
		
		Node(){
			children = new HashMap<>();
			endOfString = false;
		}
	}
	
	public Trie() {
		root = new Node();
		System.out.println("Trie is created....");
	}
	
	public void insert(String word) {
		Node current = root;
		for(int i = 0; i < word.length(); i++) {
			char c = word.charAt(i);
			Node node = current.children.get(c);
			if(node == null) {
				node = new Node();
				current.children.put(c, node);
			}
			current = node;
		}
		current.endOfString = true;
		System.out.println("Successful inserted "+word+" in this trie");
	}
	
	public boolean search(String word) {
		Node currentNode = root;
		
		for(int i = 0; i < word.length(); i++) {
			char c = word.charAt(i);
			Node node = currentNode.children.get(c);
			if(node == null) {
				System.out.println("Word: "+ word+ " does not exit in this Trie");
				return false;
			}
			currentNode = node;
			
		}
		if(currentNode.endOfString) {
			System.out.println("Word: "+ word+ " is exit in this Trie");
			return true;
		}else {
			System.out.println("Word: "+word+ " does not exist in Trie. But it is a prefix of another string");
		}
		
		return currentNode.endOfString;
	}
	
	private boolean delete(Node parentNode, String word, int index) {
		char c = word.charAt(index);
		Node currentNode = parentNode.children.get(c);
		boolean canThisNodeBeDeleted;
		
		if(currentNode.children.size() > 1) {
			delete(currentNode, word, index+1);
			return false;
		}
		
		if(index == word.length()-1) {
			if(currentNode.children.size() >= 1) {
				currentNode.endOfString = false;
				return false;
			}else {
				parentNode.children.remove(c);
				return true;
			}
		}
		
		if(currentNode.endOfString) {
			delete(currentNode, word, index+1);
			return false;
		}
		
		canThisNodeBeDeleted = delete(currentNode, word, index+1);
		if(canThisNodeBeDeleted) {
			parentNode.children.remove(c);
			return true;
		}else {
			return false;
		}
	}
	
	public void delete(String word) {
		if(search(word)) {
			delete(root, word, 0);
		}
	}
	
	

}
