package sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BucketSort {
	
	int[] arr;
	
	public BucketSort(int arr[]) {
		this.arr = arr;
	}
	
	//Print Buckets
	public void printBucket(List<Integer>[] buckets) {
		for(int i = 0; i < buckets.length; i++) {
			System.out.print("Buckets "+i+": ");
			for(int value: buckets[i]) {
				System.out.print(value+" ");
			}
			System.out.println();
		}
	}
	
	public void bucketSort() {
		int numberOfBuckets = (int) Math.ceil(Math.sqrt(arr.length));
		
		int maxValue = Integer.MIN_VALUE;
		for(int value: arr) {
			if(value > maxValue) {
				maxValue = value;
			}
		}
		
		ArrayList<Integer>[] buckets = new ArrayList[numberOfBuckets];
		
		for(int i = 0; i < buckets.length; i++) {
			buckets[i] = new ArrayList<>();
		}
		
		for(int value : arr) {
			int bucketNumber = (int) Math.ceil(((float) value * numberOfBuckets)/(float)maxValue);
			
			
			buckets[bucketNumber-1].add(value);
		}
		
		System.out.println("\n\nPrinting buckets before sorting.....");
		printBucket(buckets);
		
		//Sorting Buckets
		for(List<Integer> bucket: buckets) {
			Collections.sort(bucket);
		}
		
		System.out.println("\n\nPrinting buckets after sorting.....");
		printBucket(buckets);
		
		int index = 0;
		for(List<Integer> bucket: buckets) {
			for(int value : bucket) {
				arr[index] = value;
				index++;
			}
		}
		
	}
	
	public static void main(String[] args) {
		int[] arr = {4, 3, 5, 2, 1, 6,10,9,8};
		BucketSort bs = new BucketSort(arr);
		bs.bucketSort();
		
		System.out.println(Arrays.toString(arr));
		
	}

}
