package search;

public class BinarySearch {
	
	public static int binarySearch(int[] arr, int value) {
		int n = arr.length;
		int start = 0;
		int end = n-1;
		int middle = (start+end)/2;
		
		while(arr[middle] != value && start <= end) {
			if(value < arr[middle]) {
				end = middle-1;
			}else {
				start = middle + 1;
			}
			middle = (start+end)/2;
		}
		
		if(arr[middle] == value) {
			System.out.println(value+" is founded in "+middle+" index");
			return middle;
		}else {
			System.out.println(value+" is not found in this array");
			return -1;
		}
	}
	
	public static void main(String[] args) {
		int[] arr = {1,2,3,4,5,6,7,8,9,10,11,13,14,15,16,17};
		binarySearch(arr, 5);
		
	}

}
