package array;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ArrayUtil {
	
	public static int[] middle(int[] array) {
        // TODO
        if(array.length < 2){
            return new int[] {};
        }
        int[] newArray = new int[array.length-2];
        
        for(int i = 1; i < array.length-1; i++){
            newArray[i-1] = array[i];
        }
        
        return newArray;
    }
	
	public static int[] removeDuplicates(int[] array) {
        int n = array.length;
        int[] uniqueArray = new int[n];
        int index = 0;
 
        for (int i = 0; i < n; i++) {
            boolean isDuplicate = false;
            for (int j = i + 1; j < n; j++) {
                if (array[i] == array[j]) {
                    isDuplicate = true;
                    break;
                }
            }
            if (!isDuplicate) {
                uniqueArray[index++] = array[i];
            }
        }
 
        return Arrays.copyOf(uniqueArray, index);
    }
	
	public static int[] removeDuplicatesWithSet(int[] array) {
        Set<Integer> unique = new HashSet<>();
        
        for(int n: array) {
        	unique.add(n);
        }
		
		int[] uniqueArray = new int[unique.size()];
		int index = 0;
		for(int num:unique) {
			uniqueArray[index] = num;
			index++;
		}
		
		return uniqueArray;
    }



}
