package doublyLinkedList;

public class MainMethod {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		DoublyLinkedList dLL = new DoublyLinkedList(1);
		dLL.prepend(0);
		dLL.append(2);
		dLL.append(3);
		dLL.append(4);
		dLL.append(5);
		dLL.append(6);
		
		dLL.printList();
		
		System.out.println(dLL.get(6).value);
		dLL.getTail();
//		dLL.swapFirstLast();
		dLL.reverse();
		
		dLL.getTail();
		dLL.printList();
		
	}

}
