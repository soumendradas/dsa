package hashTable;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SubArraySum {
	
	public static int[] subarraySum(int[] nums, int target){
        Map<Integer, Integer> groupNums = new HashMap<>();
        for(int i = 0; i < nums.length; i++){
        	int num = nums[i];
            int remain = target - num;
            if(groupNums.containsKey(remain)) {
            	return new int[] {groupNums.get(remain), i};
            }else {
            	groupNums.put(num, i);
            }
        }
        return new int[] {};
    }
	
	public static void main(String[] args) {
		
		System.out.println(Arrays.toString(subarraySum(new int[] {2, 3, 4, 5, 6}, 3)));
		
	}

}
