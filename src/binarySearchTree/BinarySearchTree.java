package binarySearchTree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

import javax.swing.table.TableRowSorter;

public class BinarySearchTree {

	private Node root;

	class Node {
		int value;
		Node left;
		Node right;

		Node(int value) {
			this.value = value;
		}
	}

	public Node getRoot() {
		return root;
	}

	public boolean insert(int value) {

		Node newNode = new Node(value);
		if (root == null) {
			root = newNode;
			return true;
		}

		Node temp = root;
		while (true) {
			if (newNode.value == temp.value)
				return false;

			if (newNode.value < temp.value) {
				if (temp.left == null) {
					temp.left = newNode;
					return true;
				}
				temp = temp.left;
			} else {
				if (temp.right == null) {
					temp.right = newNode;
					return true;
				}
				temp = temp.right;
			}
		}

	}

	public boolean contains(int value) {
		Node temp = root;
		while (temp != null) {
			if (value < temp.value) {
				temp = temp.left;
			} else if (value > temp.value) {
				temp = temp.right;
			} else {
				return true;
			}
		}
		return false;
	}

	private boolean rContains(Node currentNode, int value) {
		if (currentNode == null)
			return false;

		if (currentNode.value == value)
			return true;

		if (value > currentNode.value) {
			return rContains(currentNode.right, value);
		} else {
			return rContains(currentNode.left, value);
		}

	}

	public boolean rContains(int value) {
		return rContains(root, value);
	}

	private Node rInsert(Node currentNode, int value) {
		if (currentNode == null)
			return new Node(value);
		if (currentNode.value == value)
			return null;

		if (value < currentNode.value) {
			currentNode.left = rInsert(currentNode.left, value);
		} else if (value > currentNode.value) {
			currentNode.right = rInsert(currentNode.right, value);
		}

		return currentNode;
	}

	public void rInsert(int value) {
		if (root == null) {
			root = new Node(value);
			return;
		}
		rInsert(root, value);

	}

	public Integer minValue(Node currentNode) {
		if (currentNode.left == null) {
			return currentNode.value;
		} else {
			return minValue(currentNode.left);
		}
	}

	private Node deleteNode(Node currentNode, int value) {
		if (currentNode == null)
			return null;

		if (value < currentNode.value) {
			currentNode.left = deleteNode(currentNode.left, value);
		} else if (value > currentNode.value) {
			currentNode.right = deleteNode(currentNode.right, value);
		} else if (value == currentNode.value) {
			if (currentNode.left == null && currentNode.right == null) {
				return null;
			} else if (currentNode.left == null && currentNode.right != null) {
				return currentNode.right;
			} else if (currentNode.left != null && currentNode.right == null) {
				return currentNode.left;
			} else {
				int minValue = minValue(currentNode);
				currentNode.value = minValue;
				return currentNode;
			}
		}
		return currentNode;

	}

	public void deleteNode(int value) {
		root = deleteNode(root, value);
	}

	/**
	 * 
	 * BFS: Breadth First Search
	 * 
	 * @return
	 * 
	 * 
	 */
	public List<Integer> BFS() {
		Node currentNode = root;
		Queue<Node> queue = new LinkedList<>();
		List<Integer> results = new ArrayList<>();
		queue.add(currentNode);

		while (queue.size() > 0) {
			currentNode = queue.remove();
			results.add(currentNode.value);
			if (currentNode.left != null) {
				queue.add(currentNode.left);
			}
			if (currentNode.right != null) {
				queue.add(currentNode.right);
			}
		}

		return results;

	}

	/**
	 * DFS: Depth First Search
	 * 
	 * @return
	 */
	public ArrayList<Integer> DFSPreOrder() {
		ArrayList<Integer> results = new ArrayList<>();

		class Traverse {
			Traverse(Node currentNode) {
				results.add(currentNode.value);

				if (currentNode.left != null) {
					new Traverse(currentNode.left);
				}

				if (currentNode.right != null) {
					new Traverse(currentNode.right);
				}
			}
		}

		new Traverse(root);

		return results;
	}

	public ArrayList<Integer> DFSPostOrder() {
		ArrayList<Integer> results = new ArrayList<>();

		class Traverse {
			Traverse(Node currentNode) {
				// TODO Auto-generated constructor stub
				if (currentNode.left != null) {
					new Traverse(currentNode.left);
				}

				if (currentNode.right != null) {
					new Traverse(currentNode.right);
				}
				results.add(currentNode.value);

			}
		}
		new Traverse(root);

		return results;
	}

	public ArrayList<Integer> DFSInOrder() {
		ArrayList<Integer> results = new ArrayList<>();

		class Traverse {
			Traverse(Node currentNode) {
				if (currentNode.left != null) {
					new Traverse(currentNode.left);
				}
				results.add(currentNode.value);

				if (currentNode.right != null) {
					new Traverse(currentNode.right);
				}
			}
		}

		new Traverse(root);

		return results;
	}

	public Integer kthSmallest(int k) {
		Stack<Node> stack = new Stack<>();
		
		class Traverse{
			Traverse(Node currentNode){
				
				
				if(currentNode.right != null) {
					new Traverse(currentNode.right);
				}
				stack.add(currentNode);
				
				if(currentNode.left!=null) {
					new Traverse(currentNode.left);
				}
			}
		}
		
		new Traverse(root);
		System.out.println(stack);
		
		while(k > 0) {
			stack.pop();
			k--;
		}
		
		
		return stack.pop().value;
	}

}
