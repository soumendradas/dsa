package intersection;

public class Intersection {

	public void addSameNode(LinkedList list1, LinkedList list2, int nodeValue) {
		Node newNode = new Node(nodeValue);
		list1.tail.next = newNode;
		list1.tail = newNode;
		list1.size++;
		
		list2.tail.next = newNode;
		list2.tail = newNode;
		list2.size++;
	}
	
	public Node findKthNode(Node head, int k) {
		Node current = head;
		while(k > 0 && current != null) {
			current = current.next;
			k--;
		}
		return current;
	}
	
	public Node findIntersection(LinkedList list1, LinkedList list2) {
		
		if(list1.head == null || list2.head == null) return null;
		
		if(list1.tail != list2.tail) return null;
		
		Node longer = new Node();
		Node shorter = new Node();
		
		if(list1.size > list2.size) {
			longer = list1.head;
			shorter = list2.head;
		}else {
			longer = list2.head;
			shorter = list1.head;
		}
		longer = findKthNode(longer, Math.abs(list1.size - list2.size));
		
		while(longer != shorter) {
			longer = longer.next;
			shorter = shorter.next;
		}
		return longer;
		
	}
}
