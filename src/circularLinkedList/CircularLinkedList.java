package circularLinkedList;

public class CircularLinkedList {

	Node head;
	Node tail;
	int size;
	
	class Node{
		int value;
		Node next;
		
		Node(int value){
			this.value = value;
		}
	}
	
	public CircularLinkedList(int value) {
		head = new Node(value);
		tail = head;
		head.next = head;
		size++;
	}
	
	
	public CircularLinkedList() {
		super();
	}


	public void insert(int value, int location) {
		if(head == null) {
			new CircularLinkedList(value);
			return;
		}
		
		Node newNode = new Node(value);
		if(location == 0) {
			newNode.next = head;
			head = newNode;
			tail.next = newNode;
		}else if(location >= size) {
			newNode.next = tail.next;
			tail.next = newNode;
			tail = newNode;
		}else {
			Node temp = head;
			for(int i = 0; i < location - 1; i++) {
				temp = temp.next;
			}
			
			newNode.next = temp.next;
			temp.next = newNode;
		}
		size++;
	}
	
	public void traverse() {
		/*
		 * Node temp = head; while(temp.next != head) {
		 * System.out.print(temp.value+"->");
		 * 
		 * temp = temp.next; }
		 */
		
		if(head != null) {
			Node temp = head;
			
			for(int i = 0; i < size; i++) {
				System.out.print(temp.value);
				if(i != size-1) {
					System.out.print("->");
				}
				temp = temp.next;
			}
		}
	}
	
	public boolean search(int value) {
		if(head == null) return false;
		
		Node temp = head;
		for(int i = 0; i < size; i++) {
			if(temp.value == value) {
				System.out.println("Value found in index of "+i);
				return true;
			}
			temp = temp.next;
		}
		
		
		return false;
	}
	
	public void deleteNode(int location) {
		if(head == null) {
			System.out.println("The CLL does not exist");
			return;
		}
		
		if(location == 0) {
			head = head.next;
			tail.next = head;
			size--;
			if(size == 0) {
				tail = head = null;
			}
		}else if(location >= size) {
			Node temp = head;
			
			for(int i = 0; i < size-1; i++) {
				temp = temp.next;
			}
			if(temp == head) {
				head = tail = null;
				size--;
				return;
			}
			temp.next = head;
			tail = temp;
			size--;
			
		}else {
			Node temp = head;
			for(int i = 0; i < location-1; i++) {
				temp = temp.next;
			}
			temp.next = temp.next.next;
			size--;
		}
	}
	
	public void deleteCLL(){
		if(head == null) {
			System.out.println("CLL is not exist");
		
		}else{
			head = null;
			tail.next = null;
			tail = null;
			System.out.println("The CLL is succesfully deleted");
		}
		
	}
}
