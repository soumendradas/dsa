package leetcode;

import java.util.HashMap;
import java.util.Map;

public class RomanToInteger {
	
	public int romanToInt(String s) {
		
		Map<Character, Integer> romanMap = new HashMap<>();
		
		romanMap.put('I', 1);
		romanMap.put('V', 5);
		romanMap.put('X', 10);
		romanMap.put('L', 50);
		romanMap.put('C', 100);
		romanMap.put('D', 500);
		romanMap.put('M', 1000);
		
		int result = 0;
		int prev = 0;
		
		for(int i = s.length()-1; i >= 0; i--) {
			
			int cur = romanMap.get(s.charAt(i));
			
			if(cur < prev) {
				result-=cur;
			}else {
				result += cur;
			}
			prev = cur;
			
		}
		
		return result;
	}
	
	public int romaToInt(String s) {		// different method
		
		int ans=0;
        int num=0;
        for(int i=s.length()-1;i>=0;i--){
            switch(s.charAt(i)){
                case 'I':num=1; break;
                case 'V':num=5; break;
                case 'X':num=10; break;
                case 'L':num=50; break;
                case 'C':num=100; break;
                case 'D':num=500; break;
                case 'M':num=1000; break;
            }
            if(num<ans){
                ans-=num;
            }else{
                ans+=num;
            }
        }
        return ans;
	}
	
	public static void main(String[] args) {
		RomanToInteger r = new RomanToInteger();
		System.out.println(r.romanToInt("MCMXCIV"));
		
		System.out.println(r.romaToInt("MCMXCIV"));
	}

}
