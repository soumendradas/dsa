package hashing;

public class Main {
	
	public static void main(String[] args) {
		/*
		 * DirectChaining dc = new DirectChaining(10);
		 * 
		 * System.out.println(2<<4);
		 * 
		 * dc.insertHashTable("Hello"); dc.insertHashTable("cat");
		 * dc.insertHashTable("tac"); dc.insertHashTable("dog");
		 * dc.insertHashTable("god"); dc.insertHashTable("The");
		 * dc.insertHashTable("swat"); dc.insertHashTable("cars");
		 * dc.insertHashTable("xavier");
		 * 
		 * 
		 * dc.displayHashTable(); dc.search("god"); dc.delete("cat"); dc.delete("The");
		 * dc.displayHashTable();
		 */
		
		LinearProbing dc = new LinearProbing(10);
		
		dc.insertHashTable("Hello"); dc.insertHashTable("cat");
		dc.insertHashTable("tac"); dc.insertHashTable("dog");
		dc.insertHashTable("god"); dc.insertHashTable("The");
		dc.insertHashTable("swat"); dc.insertHashTable("cars");
		dc.insertHashTable("xavier");
		 
//		dc.displayHashTable(); dc.search("god"); dc.delete("cat"); dc.delete("The");
		dc.displayHashTable();
		
	}
	

}
