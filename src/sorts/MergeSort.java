package sorts;

import java.util.Arrays;

public class MergeSort {
	
	public static void main(String[] args) {
//		int[] array1 = {1,6,7, 3, 7,8,5,4};
		int[] array2 = {5,4,6,7,1,6,7, 3, 7,8,5,4};
		
		int[] sortedArray = mergeSort(array2);
		
		System.out.println("Orginal Array: "+Arrays.toString(array2));
		System.out.println("Sorted Array: "+ Arrays.toString(sortedArray));
	}
	/**
	 * time complexity   O(n log n)
	 * Space complexity O(n)
	 * @param array
	 * @return
	 */
	public static int[] mergeSort(int[] array) {
		
		if(array.length == 1) return array;
		int midIndex = array.length/2;
		
		int[] left = mergeSort(Arrays.copyOfRange(array, 0, midIndex));
		int[] right = mergeSort(Arrays.copyOfRange(array, midIndex, array.length));
		
		return merge(left,right);
	}
	
	public static int[] merge(int[] array1, int[] array2) {
		int[] combined = new int[array1.length+array2.length];
		
		int index = 0;   //combined index
		int i = 0; 		//array1 index
		int j = 0;		//array2 index
		
		while(i < array1.length && j < array2.length) {
			if(array1[i] < array2[j]) {
				combined[index] = array1[i];
				index++;
				i++;
				
			}else {
				combined[index] = array2[j];
				index++;
				j++;
			}
		}
		
		while(i < array1.length) {
			combined[index] = array1[i];
			index++;
			i++;
		}
		
		while(j < array2.length) {
			combined[index] = array2[j];
			index++;
			j++;
		}
		
		return combined;
	}
}
