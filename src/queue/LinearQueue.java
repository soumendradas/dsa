package queue;

public class LinearQueue {
	
	private int[] arr;
	private int beginIndex;
	private int topIndex;
	
	public LinearQueue(int size) {
		arr = new int[size];
		beginIndex = -1;
		topIndex = -1;
		
	}
	
	public boolean isFull() {
		
		if(topIndex == arr.length-1) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean isEmpty() {
		if(beginIndex == -1 || beginIndex == arr.length) {
			return true;
		}else {
			return false;
		}
	}
	
	public void enQueue(int value) {
		if(isFull()) {
			System.out.println("Queue is Full");
		}else if(isEmpty()) {
			beginIndex = 0;
			topIndex = 0;
			arr[topIndex] = value;
			System.out.println("Successfull inserted "+ value+ " in the Queue");
		}else {
			topIndex++;
			arr[topIndex] = value;
			System.out.println("Successfull inserted "+ value+ " in the Queue");
		}
	}
	
	public int deQueue() {
		if(isEmpty()) {
			System.out.println("Queue is Empty");
			return -1;
		}
		int res = arr[beginIndex];
		beginIndex++;
		
		if(beginIndex > topIndex) {
			beginIndex = topIndex = -1;
		}
		return res;
	}
	
	public int peek() {
		if(isEmpty()) {
			System.out.println("Queue is Empty");
			return -1;
		}else {
			return arr[beginIndex];
		}
	}
	
	public void deleteQueue() {
		arr = null;
		beginIndex = -1;
		topIndex = -1;
		System.out.println("Queue is deleted....");
	}

}
