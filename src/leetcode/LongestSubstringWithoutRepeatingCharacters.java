package leetcode;

import java.util.HashMap;
import java.util.Map;

public class LongestSubstringWithoutRepeatingCharacters {
	
	public static void main(String[] args) {
		
		System.out.println(lengthOfLongestSubstringMap("aaabbcdef"));
		
	}
	
	public static int lengthOfLongestSubstring(String s) {
		
		if(s.trim().isEmpty()) return 0;
		
        String finalString = "";
        String currentString = "";
        
        
        for(int i = 0; i < s.length(); i++) {
        	String temp = String.valueOf(s.charAt(i));
        	if(!currentString.contains(temp)) {
        		currentString = currentString+temp;
        	}else {
        		currentString = currentString.substring(currentString.indexOf(temp)+1)+temp;
        	}
        	
        	if(currentString.length() > finalString.length()) {
        		finalString = currentString;
        	}
        }
        
        
        return finalString.length();
    }
	
	public static int lengthOfLongestSubstringMap(String s) {
		
		if(s==null || s.isEmpty()) return 0;
		
		Map<Character, Integer> map = new HashMap<>();
		
		int maxLen = 0;
		int left = 0;
		
		for(int right = 0; right < s.length(); right++) {
			char currentChar = s.charAt(right);
			
			if(map.containsKey(currentChar)) {
				left = Math.max(map.get(currentChar)+1, left);
			}
			
			map.put(currentChar, right);
			maxLen = Math.max(maxLen, right-left+1);
		}
		
		return maxLen;
		
    }
}
