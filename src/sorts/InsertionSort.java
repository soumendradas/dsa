package sorts;

import java.util.Arrays;

public class InsertionSort {
	
	public static void insertionSort(int[] arr) {
		for(int i = 1; i < arr.length; i++) {
			int temp = arr[i];
			int j = i-1;
			
			while(j > -1 && temp < arr[j]) {
				arr[j+1] = arr[j];
				arr[j] = temp;
				j--;
			}
		}
	}
	
	
	public static void main(String[] args) {
		
		int[] myArr = {5,1,9,4,3,6};
		
		insertionSort(myArr);
		System.out.println(Arrays.toString(myArr));
		
	}

}
